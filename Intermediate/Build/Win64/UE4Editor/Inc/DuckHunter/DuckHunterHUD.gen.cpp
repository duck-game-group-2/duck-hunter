// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DuckHunter/DuckHunterHUD.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDuckHunterHUD() {}
// Cross Module References
	DUCKHUNTER_API UClass* Z_Construct_UClass_ADuckHunterHUD_NoRegister();
	DUCKHUNTER_API UClass* Z_Construct_UClass_ADuckHunterHUD();
	ENGINE_API UClass* Z_Construct_UClass_AHUD();
	UPackage* Z_Construct_UPackage__Script_DuckHunter();
// End Cross Module References
	void ADuckHunterHUD::StaticRegisterNativesADuckHunterHUD()
	{
	}
	UClass* Z_Construct_UClass_ADuckHunterHUD_NoRegister()
	{
		return ADuckHunterHUD::StaticClass();
	}
	struct Z_Construct_UClass_ADuckHunterHUD_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ADuckHunterHUD_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AHUD,
		(UObject* (*)())Z_Construct_UPackage__Script_DuckHunter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADuckHunterHUD_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Rendering Actor Input Replication" },
		{ "IncludePath", "DuckHunterHUD.h" },
		{ "ModuleRelativePath", "DuckHunterHUD.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ADuckHunterHUD_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ADuckHunterHUD>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ADuckHunterHUD_Statics::ClassParams = {
		&ADuckHunterHUD::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008002ACu,
		METADATA_PARAMS(Z_Construct_UClass_ADuckHunterHUD_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ADuckHunterHUD_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ADuckHunterHUD()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ADuckHunterHUD_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ADuckHunterHUD, 3681872640);
	template<> DUCKHUNTER_API UClass* StaticClass<ADuckHunterHUD>()
	{
		return ADuckHunterHUD::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ADuckHunterHUD(Z_Construct_UClass_ADuckHunterHUD, &ADuckHunterHUD::StaticClass, TEXT("/Script/DuckHunter"), TEXT("ADuckHunterHUD"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ADuckHunterHUD);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
